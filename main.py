from sequences import *

# CavroXC,
# which causes it to home and potentially spill a drop on the deck
# convenience variables for Cavro syringe pump directions and valve states
if PRIME_CAVRO:
    CavroXC.set_state(state=CAVRO_IPA)
    CavroXC.start(volume=2, direction=CAVRO_PULL, wait=True, flowrate=4) #CAVRO_PULL up 2mL IPA into syringe
    CavroXC.set_state(state=CAVRO_PROBE) #switch valve over to CAVRO_PROBE
    CavroXC.start(volume=1.5, direction=CAVRO_PUSH, wait=True, flowrate=10) #squirt out 1.5mL to prime the line

zaber_stage.home()
zaber_stage.change_velocity(ZABER_MAX_SPEED)

#Make an array of slides
n_slides = 15
slide_array = np.full(n_slides,None)
for iSlot in range(1,n_slides+1):
    slide_array[iSlot-1] = slide_rack.get_item()

# Other arm settings
n9.armbias = 'elbow left'
n9.safe_height = 280.
n9.change_velocity(20000)
move_to_safe_origin()

#set up initial experiment parameters
initial_experiment_parameters = {
    'spin_speed':      {'min': 100.0, 'max': 5000.0, 'value': 2000.0, 'units': 'RPM', 'optimize_with_chem_os':True},
    'spin_time':       {'min': 10.0 , 'max': 120.0 , 'value': 60.0  , 'units': 's'  , 'optimize_with_chem_os':False},
    'dispense_volume': {'min': 0.001, 'max': 1.0   , 'value': 0.150 , 'units': 'mL' , 'optimize_with_chem_os':True}
}

time.sleep(10)

#iteratively perform experiments
for iteration_count, active_slide in enumerate(slide_array):

    #get next experiment parameters from ChemOS
    experiment_parameters = get_experiment_parameters_from_chem_os(initial_experiment_parameters, iteration_count)

    #make a new mixtures
    #make_new_mixture()

    #execute the experiment
    spin_coat(experiment_parameters,iteration_count, active_slide) #START DEBUGGING HERE

    #image film & analyze image
    image_film(experiment_parameters, iteration_count, active_slide)

    #perform conductivity measurement
    measure_conductivity(experiment_parameters, iteration_count, active_slide)