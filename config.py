import time
import os
import numpy as np

from ada_core.dependencies.coordinates import Location
from ada_core.dependencies.arm import robotarm
from ada_core.dependencies.webcam import PhotoBooth
from ada_core.dependencies.simulator import Asset, simulator
from ada_core.components.pumps import CavroPump
from ada_core.components.manipulated import Tray, OnDeckPneumaticHolder, SpinCoater, TipRemover
from ada_core.profiles.syringes import UNF2a_2_5mL_3cm
from ada_core.profiles.vials import twenty_mL
from ada_core.profiles.tip_removers import integrated_gen2
from ada_core.profiles.trays import needle_tray_gen2

from ada_peripherals.zaber.Zaber_XLDQ1000C import NewZaberXLDQ1000C
from ada_peripherals.keithley.k2636b import k2636b

from components import SpinCoaterSuction, SpinCoaterCover, SlideRack, SlideHandler
import assets


os.environ['IMAGEFOLDER'] = './tmp'

SIMULATE = True
CLEAN_NEEDLE = True
PRIME_CAVRO = False
ZABER_MAX_SPEED = 1000

CAVRO_IPA = 0
CAVRO_PROBE = 1
CAVRO_PULL = 1
CAVRO_PUSH = 0

#
# Profiles
#

half_big_half_small_tray_profile = {  # index is where you screw it into deck close to the large vial
    'height': 27.,  # is acutally more like 25
    'spacing': 30,  # hole to hole spacing
    'fliprow': False,  # rows increase towards robot
    'rows': 1,
    'columns': 4,
    'x_offset': -25.,
    'y_offset': 0,
    'itemclass': 'Vial',
}

HPLC_2mL_pierce = {  # 2 mL HPLC vial with a piercable cap
    'capdiameter': 11.51,
    'capheight': 5.82,
    'crimp': False,
    'diameter': 11.62,
    'height': 33.41,
    'screw': True,
    'piercediameter': 0., #5.97,
    'screwpitch': 1.07,
    'rotations': 4,
    'piercedepth': 3.,
    'maximum_volume': 1.7,
}

vial_tray_gen1 = {  # tray of vials
    'height': 39.,  # height where the items will be placed, changed by BPM on 20180902 from 25mm to 39mm because 14mm spacer were added
    'spacing': 20.,  # center-center spacing
    'x_offset': -4,  # vial center update from -5mm to -4mm
    'y_offset': -145.,
    'rows': 8,
    'columns': 12,
    'itemclass': 'Vial',
    'asset': assets.vial_tray_gen1
}

pipette_tip_tray_gen1 = {
    'spacing': 10.,  # hole to hole spacing
    'height': 99.+25,  # height from base to top
    'fliprow': True,  # rows increase towards robot
    'x_offset': 10.+4,
    'y_offset': 10.+1.8,
    #'z_offset': 38.5,
    'rows': 3,
    'columns': 7,
    'itemclass': 'Needle',
    'additional_offsets': [
        {
            'start': 'B1',
            'stop': 'B7',
            'z': 3.,
        },
        {
            'start': 'C1',
            'stop': 'C7',
            'z': 1.,
        }
    ],
}

needle_plate_config = {
    'spacing': 15.512,  # hole to hole spacing
    'height': 125,  # height from base to top
    'fliprow': True,  # rows increase towards robot
    'x_offset': 14. - 7.848 + 10 + 30 - 14.99 - 3.606 - 1,
    'y_offset': 61.8 - 32.325 + 10 - 7.252 - 1,
    #'z_offset': 38.5,
    'rows': 1,
    'columns': 6,
    'itemclass': 'Needle',
}

vial_gripper_gen1_with_foam = {
    'height': 85. + 11,  # the height of the bottom of the gripper from the robot deck to the bottom of the actuating bit
    'x_offset': 0.,  # the x offset from the index
    'y_offset': -7.,  # the y offset from the index
}

#
# Instances
#

n9 = robotarm

spin_coater = SpinCoater(
    axis=4,
    location='B16',
    output=2,
    cpr=400,
    asset=assets.spin_coater
)

spin_coater_suction = SpinCoaterSuction(
    output=2,
)

spin_coater_cover = SpinCoaterCover(
    stand_index='H20',
    coater_index='E16',
    on_coater=True,
    asset=assets.spin_coater_lid,
)

slide_handler = SlideHandler(
    location='O15',
    output=1,
    height=99.,
    thickness=6.
)

slide_rack = SlideRack(
    location='P11'
)

slide_imaging_camera = PhotoBooth(
    cam=0
)

conductivity_probe_asset = Asset(Location('P13'), assets.conductivity_probe)

zaber_stage = NewZaberXLDQ1000C(
    simulate=SIMULATE,
    simulator=simulator,
    simulator_asset=Asset(Location('M19'), assets.zaber_1000),
)

CavroXC = CavroPump(
    comport=2,
    address='/1',
    **UNF2a_2_5mL_3cm,
)

vial_tray = Tray(
    location='N5',
    **vial_tray_gen1,
    itemkwargs=HPLC_2mL_pierce
)
vial_array = [vial_tray[f'A{i}'] for i in range(1, 13)]

stock_tray = Tray(
    location='O5',
    **half_big_half_small_tray_profile,
    itemkwargs=twenty_mL,
    asset=assets.half_big_half_small_tray,
)

on_deck_holder = OnDeckPneumaticHolder(
    location='K3',
    output=3,
    **vial_gripper_gen1_with_foam,
)

remover = TipRemover(
    location='C6',
    smidge=1.,
    **integrated_gen2
)

needles = Tray(
    location='C6',
    **needle_tray_gen2,
    itemkwargs={
        'gauge': 21,
        'bd_length': 50.,
    },
    asset=assets.needle_rack,
)
