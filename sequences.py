import os
import warnings
import matplotlib as mpl
import matplotlib.pyplot as plt
from config import *

def rotate_toward_Z_column(v=1000): #TODO: this method is out of date now that gripper has no symmettry
    n9.move_to_location({}, aligngripper=0, v=v) #0 faces away from Z-column
    n9.move_to_location({}, aligngripper=-60, v=v) #negative signs to ensure ccw rotation
    n9.move_to_location({}, aligngripper=-120, v=v)
    n9.move_to_location({}, aligngripper=-180, v=v) #180 faces toward Z-column


def rotate_away_from_Z_column(v=1000): #TODO: this method is out of date now that gripper has no symmettry
    n9.move_to_location({}, aligngripper=-180, v=v) #180 faces toward Z-column
    n9.move_to_location({}, aligngripper=-120, v=v)
    n9.move_to_location({}, aligngripper=-60, v=v)
    n9.move_to_location({}, aligngripper=0, v=v) #0 faces away from Z-column


def slide_from_holder_to_zaber_stage():

    move_to_safe_origin()

    #The follow sequence moves above the tray, drops down to deposit the slide in the tray, moves the handler out of the tray then moves up to safeheight
    n9.move_to_location({'x': -285.5, 'y': 40, 'z':120},aligngripper=0)  #go above the slide tray on the zaber stage
    n9.move_to_location({'x': -285.5, 'y': 40, 'z': 95}, aligngripper=0) #go down into the slide tray but don't contact the slide to the tray
    slide = slide_handler.off()                                                  #turn off slide handler suction
    n9.move_to_location({'x': -285.5, 'y': 40, 'z': 92.5}, aligngripper=0)
    time.sleep(0.2)
    slide.item_asset.attach('zaber')
    time.sleep(0.2)
    n9.move_to_location({'x': -285.5, 'y': 40, 'z': 80}, aligngripper=0) #drop below the gripper/slide/tray contact point (contact @ ~Z=92.5), leaving the slide behind on the tray
    for yPos in [40,35,30,25,20,10]:     #withdraw the slide handler in the Y-direction out of the tray
        n9.move_to_location({'x': -285.5, 'y': yPos, 'z': 80}, aligngripper=0)  # , 'z': 115.0})

    n9.safeheight()
    move_to_safe_origin()


def pick_up_slide_from_zaber_stage(slide):

    move_to_safe_origin()

    # The following sequence moves the slide handler infront of the tray below the slide height, then underneath the slide, then rises up, picking up the slide then moves up to safeheight
    n9.move_to_location({'x': -285.5, 'y': 10, 'z': 80}, aligngripper=0) #move infront of the tray
    for yPos in [10,20,25,30,35,40]:  #move the slide handler underneath the slide on the tray
        n9.move_to_location({'x': -285.5, 'y': yPos, 'z': 80}, aligngripper=0)
    n9.move_to_location({'x': -285.5, 'y': 40, 'z': 90}, aligngripper=0)  # rise up to just below below the gripper/slide/tray contact point (contact @ ~Z=92.5)
    slide_handler.on(slide)                                                    # turn on the slide handler suction
    n9.move_to_location({'x': -285.5, 'y': 40, 'z': 92.5}, aligngripper=0)
    time.sleep(0.2)
    slide.item_asset.attach(slide_handler.asset.id)
    time.sleep(0.2)
    n9.move_to_location({'x': -285.5, 'y': 40, 'z': 120}, aligngripper=0) # move upwards, picking up the slide off the tray
    n9.safeheight()

    move_to_safe_origin()


def slide_from_holder_to_spin_coater(): #this subroutine can tangle the slide handler air hose due to ackward twisting of the hose

    move_to_safe_origin()

    # Rotate slide 180
    rotate_toward_Z_column()

    # Approach front of coater
    n9.move_to_location({'x': -226.99, 'y': 70, 'z': 72.0}, aligngripper=180)

    # Move into coater.
    for y_loc in range(60, -30, -10):
        n9.move_to_location({'y': y_loc}, aligngripper=180)

    # Final y location
    n9.move_to_location({'y': -33}, aligngripper=180)

    # Close z approach to spin coater chuck
    n9.move_to_location({'z': 71}, aligngripper=180)

    # Align coater
    spin_coater.align('y')

    # Turn on spin coater vacuum
    spin_coater_suction.on()

    # Turn off vacuum on
    slide = slide_handler.off()

    time.sleep(0.2)
    slide.item_asset.attach('spin_coater_joint')
    time.sleep(0.2)
    # Lower slide_handler below slide for hand off
    n9.move_to_location({'z': 68}, aligngripper=180)

    # Turn off the spin coater vacuum because it's loud
    spin_coater_suction.off()

    # Iteratively move away from spin coater
    for y_loc in range(-30, 20, 10):
        n9.move_to_location({'y': y_loc}, aligngripper=180)

    # Rotate slide handler to face front of deck
    n9.move_to_location({'z':250}, aligngripper=180) #move up high to avoid hitting the spin coater lid with the slide handler when unwinding
    rotate_away_from_Z_column(v=10000)


def slide_from_coater_to_holder(slide):

    move_to_safe_origin()

    #align the slide to the spincoater
    spin_coater_suction.on() #hold down the slide otherwise it will slip off the chuck
    spin_coater.align(alignment=0) #realign the spincoater so that the slide handler can pick it up properly irrespective of the total rotation during the spin coating step
    spin_coater_suction.off() #release the slide in preperation for slide removal by the slide handler

    # Approach front of coater and line up slide with coater (should really split into align, then rotate for safety)
    n9.move_to_location({'x': -226.99, 'y': 10, 'z': 68.0}, aligngripper=180)

    # Move into coater.
    for y_loc in range(10, -30, -10):
        n9.move_to_location({'y': y_loc}, aligngripper=180)

    # Final y location
    n9.move_to_location({'y': -33}, aligngripper=180)

    # Ensure that the spin coater vacuum is off
    spin_coater_suction.off()

    # Turn handler vacuum on
    slide_handler.on()

    # Close z approach to spin coater chuck
    n9.move_to_location({'z': 71}, aligngripper=180, v=2000)
    time.sleep(0.2)
    slide.item_asset.attach(slide_handler.asset.id)
    time.sleep(0.2)

    # Iteratively move away from spin coater
    n9.move_to_location({'y': -20}, aligngripper=180)

    # for y_loc in range(-20, 80, 10):
    #     n9.move_to_location({'y': y_loc}, aligngripper='y')

    # Safe height
    n9.move_to_location({'z': 250})

    # Rotate back
    # rotate_backward()

    # Change state of slide_holder
    slide_handler.ingrip = slide


def move_to_safe_origin():
    n9.move_to_location({'z': 280})
    n9.move_to_location({'x' : 0, 'y' : 100}, aligngripper=0) #BPM added aligngripper=0 here for esthetic purposes on 20180908


def move_needle_into_coater():

    # Move to safe height
    n9.move_to_location({'z': 280})

    # Move above the hole in the lid
    n9.move_to_location({'x': -209.8348, 'y': -84.1901})

    # Approach slide
    n9.move_to_location({'z': 122.})

    # VERY close approach comemnted below
    # n9.move_to_location({'z': 119.})


def move_needles_out_of_coater():

    # Move to safe height
    n9.move_to_location({'z': 160})


def rinse(volume=0):
    n9.move_to_location('H7', target='probe')
    n9.move_to_location({'z': 80.})
    CavroXC.draw_and_dispense(volume, valveinput=0, valveoutput=1, backdraw=False, wait=True)
    n9.move_to_location({'z': 110.})
    for i in range(3):
        n9.jerk()


def performk2636bMeasurement(iter=1, **kwargs):
    import time
    listed_df = []
    for i in range(iter):
        keithley = k2636b(address='ASRL3::INSTR')
        current_df = keithley.runScript(
            script_name='voltageSweep',
            **kwargs
        )

        listed_df.append(current_df)
        time.sleep(1)
        keithley.closeConnection()

    return listed_df


def metricPrefix(num):
    # Define prefixes
    prefixes = {
        'Y': 1e24,
        'Z': 1e21,
        'E': 1e18,
        'P': 1e15,
        'T': 1e12,
        'G': 1e9,
        'M': 1e6,
        'k': 1e3,
        '': 1e0,
        'm': 1e-3,
        'μ': 1e-6,
        'n': 1e-9,
        'p': 1e-12,
        'f': 1e-15,
        'a': 1e-18,
        'z': 1e-21,
        'y': 1e-24,
    }

    # Keep floor dividing until suitable SI prefix is found
    found = False
    for symbol, multiplier in prefixes.items():
        if num // multiplier > 0:
            found = True
            break

        # Test to make sure a number was found
    if found == False:
        # raise ValueError('No suitable SI prefix was found for the provided number. This is likely because the'
        #                  'number is smaller than 1e-24.')
        multiplier = 1
        symbol = ''

    return multiplier ** -1, symbol


def processAndCombineDataframes(raw_dfs):
    processed_conductivity_data = {}

    # Combine dataframes
    for count, df in enumerate(raw_dfs):
        if count == 0:
            raw_data_merged_df = df
            raw_data_merged_df.columns = ['voltage_V', 'current_A_0']
        else:
            raw_data_merged_df = raw_data_merged_df.merge(df, on='voltage_V', suffixes=['',
                                                                                        f'_{str(count)}'])  # TODO: this merge doesn't names the 2nd column current_A instead of current_A_1

    # Calculate mean and std, and add to dataframe
    currents = raw_data_merged_df.drop(['voltage_V'], axis=1)
    raw_data_merged_df['mean'] = currents.mean(axis=1)
    raw_data_merged_df['std'] = currents.std(axis=1)

    # Linear fit
    fit = np.polyfit(raw_data_merged_df['voltage_V'], raw_data_merged_df['mean'], 1)
    processed_conductivity_data['slope'], processed_conductivity_data['intercept'] = fit
    processed_conductivity_data['fit_fn'] = np.poly1d(fit)
    processed_conductivity_data['resistance'] = processed_conductivity_data['slope'] ** -1

    # Plot bounds
    processed_conductivity_data['x_min'] = raw_data_merged_df['voltage_V'].min()
    processed_conductivity_data['x_max'] = raw_data_merged_df['voltage_V'].max()
    processed_conductivity_data['y_min'] = currents.values.min()
    processed_conductivity_data['y_max'] = currents.values.max()

    # Calculate ranges and buffers
    processed_conductivity_data['x_range'] = processed_conductivity_data['x_max'] - processed_conductivity_data['x_min']
    processed_conductivity_data['y_range'] = processed_conductivity_data['y_max'] - processed_conductivity_data['y_min']
    processed_conductivity_data['x_buffer'] = processed_conductivity_data['x_range'] * 0.05
    processed_conductivity_data['y_buffer'] = processed_conductivity_data['y_range'] * 0.05

    # Calculate the metric multipliers
    processed_conductivity_data['x_factor'], processed_conductivity_data['x_symbol'] = metricPrefix(
        processed_conductivity_data['x_range'])
    processed_conductivity_data['y_factor'], processed_conductivity_data['y_symbol'] = metricPrefix(
        processed_conductivity_data['y_range'])

    return raw_data_merged_df, processed_conductivity_data


def performConductivityAnalysis():
    # Perform the experiment
    raw_conductivity_dfs = performk2636bMeasurement(iter=3, delay=0.05, v_start=-0.01, v_end=0.01, v_step=0.002,
                                                    nplc=10)
    # Process the data
    raw_conductivity_merged_df, processed_conductivity_data = processAndCombineDataframes(raw_conductivity_dfs)

    return raw_conductivity_dfs, raw_conductivity_merged_df, processed_conductivity_data


# Define spin-coating subroutine
def spin_coat(experiment_parameters, iteration_count, active_slide):
    '''
    This subroutine spin-coats a slide according to the parameters in the 'experiment_parameters' dictionary
    The required starting state is as follows:
        *spin-coater cover on the spin coater
        *slides available in the slide storage rack
        *needles available in the needle rack
        *spin-coating solution in a vial at position A1
        *slide handler on the slide handler station
        *nothing in the gripper

    The final state should be as follows:
        *nothing in gripper
        *spin coated slide in rack
        *spin coater lid on the spin coater

    :param experiment_parameters dictionary containing the parameters for the spin coating experiment (e.g. spin speed, dispense volume)
    :param iteration_count:
    :param active_slide: which slide to take from the slide storage rack
    :return: none
    '''



    # Put slide on coater, and place cover on coater
    move_to_safe_origin()
    spin_coater_cover.move_from_coater_to_stand()
    slide_handler.pick_up()
    slide_handler.pick_up_slide(active_slide, z_span=3)
    slide_from_holder_to_spin_coater()
    slide_handler.put_down()
    spin_coater_cover.move_from_stand_to_coater()
    move_to_safe_origin()

    # Get a fresh needle, pick up solution and position needle over coater
    #TODO: consider adding a small (~0.050 mL) air draw before pulling up the solution
    nd = needles.get_item()
    nd.retrieve_tip()
    n9.invert_elbow() #warning! inverts somewhere later in the script; this line is neccesary to avoid elbow collision with the needle rack
    n9.move_to_location({'z': 280})
    #TODO: replace pulling from small vial with pulling from big stock bottle
    n9.move_to_location(  # move arm to stock location
        stock_tray['A1'].get_location('bottom'),  # stock tray profile is not up to date, so needle doesnt actually go to the bottom
        target='probe',
    )
    # n9.move_to_location(vial_array[iteration_count].get_location('pierce'), target='probe') #TODO: fix needle/vial XY
    # alignment (off center) and Z alignment (hits vial bottom),
    CavroXC.set_state(state=CAVRO_PROBE)
    CavroXC.start(volume=0.05 + experiment_parameters['dispense_volume']['value'], direction=CAVRO_PULL, wait=True, flowrate=10) #0.05 mL added to CAVRO_PULL volume
    time.sleep(3) #allow time for aspirated liquid pressure to equilibrate

    move_to_safe_origin()
    #TODO: move the above invert elbow (~line 1175) here?
    move_needle_into_coater()

    # Spin coat: spin the coater, dispense, retract needle
    spin_coater_suction.on()
    spin_coater.spin(t=experiment_parameters['spin_time']['value'], rpm=experiment_parameters['spin_speed']['value'], wait=False) #execute the spin coating step #default time was previously 70, changed to 10 for quick testing
    time.sleep(1)
    CavroXC.set_state(state=CAVRO_PROBE)
    CavroXC.start(volume=experiment_parameters['dispense_volume']['value'], direction=CAVRO_PUSH, wait=True, flowrate=100)
    move_needles_out_of_coater()
    move_to_safe_origin()

    # Clean the needle/CAVRO_PROBE by purging
    if CLEAN_NEEDLE:
        move_to_safe_origin()
        n9.move_to_location({'z': 200.})
        n9.move_to_location('N4', target='probe') #changed to new position of waste beaker
        n9.move_to_location({'z': 80.})
        if not SIMULATE:
            for i in range(3):
                CavroXC.set_state(state=CAVRO_IPA)
                CavroXC.start(volume=1, direction=CAVRO_PULL, wait=True, flowrate=5)
                time.sleep(3)
                CavroXC.set_state(state=CAVRO_PROBE)
                CavroXC.start(volume=1, direction=CAVRO_PUSH, wait=True, flowrate=50)
        move_to_safe_origin()
    else:
        time.sleep(experiment_parameters['spin_time']['value']) #wait for spin coating to finish

    #remove the needle from the CAVRO_PROBE
    remover.remove_tip()
    move_to_safe_origin()

    # align the slide to the spincoater
    spin_coater_suction.on()  # hold down the slide otherwise it will slip off the chuck
    spin_coater.align(alignment=0)  # realign the spincoater so that the slide handler can pick it up properly irrespective of the total rotation during the spin coating step
    spin_coater_suction.off()  # release the slide in preperation for slide removal by the slide handler


    # Retrieve the coated slide from the spin coater and put it away
    spin_coater_suction.off()
    spin_coater_cover.move_from_coater_to_stand() #change move z to safe height 280
    slide_handler.pick_up()
    slide_from_coater_to_holder(active_slide) # this command moves the handler in a funny way
    move_to_safe_origin()
    slide_handler.put_away_slide(slide_rack.get_location(iteration_count + 1), 0, z_span=3)
    slide_handler.put_down()
    spin_coater_cover.move_from_stand_to_coater()
    move_to_safe_origin()
    n9.invert_elbow() #this line is crucial - it leaves the arm in the same state as it began in to avoid gripper alignment issues


# Define conductivity measurement subroutine
def measure_conductivity(experiment_parameters, iteration_count, active_slide): #TODO: test this function
    # pick up the slide
    slide_handler.pick_up()
    slide_handler.pick_up_slide(active_slide, z_span=3)

    # move the slide under conductivity station
    n9.move_to_location({'x': -91.5}, aligngripper='y')
    n9.move_to_location({'z': 60}, aligngripper='y')
    for y_val in range(190, 260, 10):
        n9.move_to_location({'y': y_val}, aligngripper='y')
    n9.move_to_location({'y': 259}, aligngripper='y')

    # Perform conductivity measurement
    processed_conductivity_map = []
    raw_conductivity_map = []
    for x in range(7):
        # move up to bring the film into contact with the 4-point CAVRO_PROBE
        n9.move_to_location({'z': 68}, v=500) #A height of Z = 68 makes somewhat marginal contact to the 4-point CAVRO_PROBE pins

        # make the conductivity measurement
        if not SIMULATE:
            raw_conductivity_dfs, raw_conductivity_merged_df, processed_conductivity_data = performConductivityAnalysis()

            processed_conductivity_map.append(processed_conductivity_data)
            raw_conductivity_map.append(raw_conductivity_merged_df)

            # Send the resulting conductivity data to the plotter
            # TODO Turn off this list pickle, and send to the plotter
            metadata_dict = {'sample_number': iteration_count, 'position_mm': 5 * (x) }
            plotter.update(data_type='data',data_metadata = metadata_dict, data = raw_conductivity_dfs)


        # move out of contact then move to the next grid point
        n9.move_to_location({'z': 60}, aligngripper='y')
        n9.move_to_location({'y': 259 - 5 * (x + 1)}, aligngripper='y')

    # Now that conductivity measurement is complete, move away from conductivity station
    for y_val in range(250, 180, -10):
        n9.move_to_location({'y': y_val}, aligngripper='y')
    n9.move_to_location({'y': 180}, aligngripper='y')

    # Put away the slide handler and move back to the safe origin
    slide_handler.put_away_slide(slide_rack.get_location(iteration_count + 1), 0, z_span=4)
    slide_handler.put_down()
    move_to_safe_origin()


# Define slide imaging & analysis subroutine
def image_film(experiment_parameters, iteration_count, active_slide):

    # formulate a file name to store data into
    RPM = experiment_parameters['spin_speed']['value']
    dispense_volume = experiment_parameters['dispense_volume']['value']
    image_file_name = f'film_image_{RPM}_RPM_dispense_vol_{dispense_volume}_mL.jpg' #TODO: add spin time to image file name

    #move stage to slide handoff position
    try:
        slide_handoff_position_in_mm_from_home = 1000
        zaber_stage.move_absolute(slide_handoff_position_in_mm_from_home)
    except:
        print('zaber stage has thrown an error')

    #pickup slide from storage
    slide_handler.pick_up()
    slide_handler.pick_up_slide(active_slide, z_span=3)

    #put the slide on the zaber stage
    slide_from_holder_to_zaber_stage()

    #move the stage under the camera
    try:
        imaging_position_in_mm_from_home = 0
        zaber_stage.move_absolute(imaging_position_in_mm_from_home)
    except:
        print('zaber stage has thrown an error')

    if not SIMULATE:
        #image the film
        film_image = slide_imaging_camera.take_picture(cam = 0) #cam = 0
        film_image = film_image[:,:,::-1] #convert the image from BGR to RGB color order
        # plt.imshow(film_image)
        # plt.show() #this line may be needed to show the image in debugger mode

        #save the image
        script_path = os.path.dirname(os.path.realpath(__file__)) #the photobooth module changes the working directory, so need to find the script root directory
        os.chdir(script_path) #change back to the script root directory
        plt.imsave(fname = image_file_name, arr = film_image, format = 'jpg')

        #analyze the film image to produce a homogeneity score
        #TODO: use Pierre's code here
        #class_coverage = AnalyseCoverage((script_path + '\\' + image_file_name), script_path, image_file_name)
        #class_coverage.analysis()

    #return the stage to the handoff position
    try:
        slide_handoff_position_in_mm_from_home = 1000
        zaber_stage.move_absolute(slide_handoff_position_in_mm_from_home)
    except:
        print('zaber stage has thrown an error')

    #pick up the slide off the zaber stage with the N9
    pick_up_slide_from_zaber_stage(active_slide)

    #put the slide away in the rack
    slide_handler.put_away_slide(slide_rack.get_location(iteration_count + 1), 0, z_span=4)
    slide_handler.put_down()

    #finish at the safe origin
    move_to_safe_origin()


# Define subroutine for getting spin-coating speed & dispense amount from ChemOS
def get_experiment_parameters_from_chem_os(initial_experiment_parameters, iteration_count):
    experiment_parameters = initial_experiment_parameters #start by copying initial parameters

    #obtain the parameters #TODO: implement getting parameters from chemOS
    spin_speed = 500 + (iteration_count/5) * (2400-500) #[RPM]
    #dispense_volume = 0.150# [mL]

    #update the experiment parameters
    experiment_parameters['spin_speed']['value'] = spin_speed
    #experiment_parameters['dispense_volume']['value'] = dispense_volume

    return experiment_parameters
