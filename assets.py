from os import path

zaber_1000 = {
    'file': path.abspath('./models/zaber_1000_base.fbx'),
    'name': 'zaber_base',
    'model_rotation': 90,
    'color': '#222222',
    'children': [{
        'file': path.abspath('./models/zaber_1000_slider.fbx'),
        'name': 'zaber',
        'model_rotation': 90,
        'color': 'white',
        'joint': {
            'type': 'prismatic',
            'input_range': [0, 1000000000],
            'output_range': [-1000, 0],
            'axis': [1, 0, 0],
        },
        'children': [{
            'file': path.abspath('./models/zaber_slide_holder.fbx'),
            'model_rotation': 90,
        }]
    }]
}

conductivity_probe = {
    'file': path.abspath('./models/conductivity_probe.fbx'),
    'model_rotation': 180
}

slide_handler = {
    'file': path.abspath('./models/slide_handler.fbx'),
    'model_location': {'z': 4}
}

slide_handler_base = {
    'file': path.abspath('./models/slide_handler_base.fbx'),
    'model_scale': {'z': 1.065}
}

spin_coater = {
    'file': path.abspath('./models/spin_coater.stl'),
    'model_location': {'x': -38.2, 'y': 17},
    'model_rotation': -90,
    'model_scale': {'z': 0.96},
    'children': [{
        'type': 'cube',
        'name': 'spin_coater_joint',
        'location': {'x': -38.2, 'y': 58, 'z': 75},
        'joint': {
            'type': 'revolute'
        }
    }]
}

spin_coater_lid = {
    'name': 'spin_coater_lid_base',
    'file': path.abspath('./models/spin_coater_lid.stl'),
    'material': 'Glass',
    'model_rotation': {'x': 90},
    'model_location': {'z': -80},
    'model_scale': {'y': 1.1}
}

slide_rack = {
    'file': path.abspath('./models/slide_rack.stl'),
    'model_location': {'y': 32.5},
    'model_rotation': {'z': 180}
}

vial_tray_gen1 = {
    'file': path.abspath('./models/vial_tray_gen1.fbx'),
}

half_big_half_small_tray = {
    'file': path.abspath('./models/half_big_half_small_tray.fbx'),
}

needle_rack = {
    'file': path.abspath('./models/needle_rack.stl'),
    'model_location': {'x': 62.5, 'y': -43.7}
}