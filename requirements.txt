git+https://gitlab.com/ada-chem/ada_core.git@stable#egg=ada_core
git+https://gitlab.com/ada-chem/ada_peripherals.git@master#egg=ada_peripherals
git+https://gitlab.com/ada-chem/ftdi_serial@master#egg=ftdi_serial
git+http://gitlab.com/larsyunker/unithandler.git@master#egg=unithandler
git+https://github.com/theskumar/python-dotenv.git
zaber.serial