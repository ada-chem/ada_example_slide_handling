Ada Slide Handling Demo
=======================

This is a demo project showing off a fairly complex application built on `ada_core`. This project is split up into 
the following files:

|file|description|
|---|---|
|main.py|The entry point to the application|
|config.py|Configures instances of "components" (usually attached to the robot's deck)|
|assets.py|Configures asset definitions used to show components in the simulator|
|components.py|Contains any application-specific components|
|sequences.py|Contains higher-level "sequences", which are methods that contain low-level robot movements|
|robot_parameters.py|Robot-specific configuration and calibration parameters|

Installation
------------

1. Clone this project into a directory on your local computer
1. Open the cloned project in PyCharm, or navigate to the cloned directory
1. Install the project requirements using the PyCharm popup, or manually with `pip install -r requirements.txt`
1. Download the latest release of the [N9 Simulator](https://gitlab.com/ada-chem/n9_simulator/-/archive/v0.1.0/n9_simulator-v0.1.0.zip) and extract (there is no installation needed)

Running
-------

1. Run `N9 Simulator` inside the simulator extracted directory
1. Run `main.py` in PyCharm or manually by running `python main.py`