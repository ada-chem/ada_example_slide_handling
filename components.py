import time
import numpy as np
from ada_core.dependencies.coordinates import Location
from ada_core.dependencies.arm import robotarm
from ada_core.dependencies.simulator import Asset
from ada_core.components.manipulated import PneumaticHolder
from ada_core.items.basic import Item
import assets


class Slide(Item):
    def __init__(self,
                 location,
                 thickness=2.,
                 span=75.,
                 width=25.,
                 orientation=0.,
                 pickup_point=20.,
                 origin_offset=10,
                 **kwargs,
                 ):
        """


        :param location: location of the center of the slide
        :param thickness: thickness of the slide (mm)
        :param span: length of the long edge (mm)
        :param width: width of the slide (short edge)
        :param orientation: orientation relative to the y-axis of the robot deck (degrees)
        :param pickup_point: the distance in from the edge of the slide to pick up the slide from (mm)
        :param kwargs:
        """
        asset = {
            'type': 'cube',
            'material': 'Glass',
            'model_scale': {'x': width, 'y': span, 'z': thickness},
            'model_location': {'x': width/2, 'y': -span/2 + origin_offset, 'z': -thickness * 1.5}
        }

        Item.__init__(self,
                      location,
                      length=thickness,
                      asset=asset,
                      **kwargs,
                      )
        self.span = span  # span of the slide (length of the long edge)
        self.width = width  # width of the slide
        self.orientation = orientation
        self.pickup_point = pickup_point

    def __repr__(self):
        return f'{self.__class__.__name__}({self.get_location()}, {self.orientation})'

    def __str__(self):
        return repr(self)


class SlideRack(object):
    def __init__(self,
                 location,
                 stacks=15,
                 spacing=14., #number to be doublechecked TONIGHT
                 x_offset=0.,
                 y_offset=37.5/2,
                 z_offset=83.,
                 population=None,
                 slidekwargs=None,
                 iterorder='up',
                 asset=assets.slide_rack,
                 ):
        """
        A class describing a rack of glass slides

        :param location: location of the rack index
        :param stacks: number of slides stacked in the vertical direction
        :param spacing: spacing between the slides (mm)
        :param x_offset: x offset from index to the center of the first slide (mm)
        :param y_offset: y offset from index to the center of the first slide (mm)
        :param z_offset: z offset from index to the bottom of the first slide (mm)
        :param population: population of slides
        :param slidekwargs: keyword arguments for slide initialization
        :param iterorder: whether to iterate up or down when retrieving items
        """
        # todo consolidate with Tray (or subclass Tray)
        # todo modify locationarray to accept stacks
        self.asset = Asset(location, asset)
        self.iterorder = iterorder
        self.location = Location(location)
        self.location.offset(
            x=x_offset,
            y=y_offset,
            z=z_offset,
        )
        self._array = [  # array of locations
            Location(
                x=self.location.x,
                y=self.location.y,
                z=self.location.z + offset,
            ) for offset in np.linspace(0., (stacks-1) * float(spacing), stacks)
        ]

        self.itemarray = np.full((len(self._array)), None)  # unfilled item array
        if slidekwargs is None:
            slidekwargs = {}
        if population is not None:
            pass  #todo
        else: #if the population is none
            for slot, loc in enumerate(self.itemarray):
                self.itemarray[slot] = Slide(
                    self._array[slot],
                    **slidekwargs,
                )

        self._generator = self._yielditem()

    def __repr__(self):
        return f'{self.__class__.__name__}({self.location})'

    def __str__(self):
        return f'{self.__class__.__name__}'

    def __len__(self):
        return sum([1 for item in self if item is not None])

    def __iter__(self):
        if self.iterorder == 'up':
            for slide in self.itemarray:
                if slide is not None:
                    yield slide
        elif self.iterorder == 'down':
            for slide in self.itemarray[::-1]:
                if slide is not None:
                    yield slide

    def _yielditem(self):
        """create a generator array"""
        if self.iterorder == 'up':
            for slide in self.itemarray:
                if slide is not None:
                    yield slide
        elif self.iterorder == 'down':
            for slide in self.itemarray[::-1]:
                if slide is not None:
                    yield slide

    def get_item(self):
        """Returns the next slide in the stack"""
        try:
            return next(self._generator)
        except StopIteration:
            raise TrayEmpty

    def get_empty_location(self):
        """"""
        pass #todo

    def item_in_location(self, slot, slide=None):
        """"""
        pass #todo

    def remove_item_from_location(self, slot):
        """"""
        pass # todo

    def get_location(self, slot):
        """
        Returns the location of the slot number in the SlideRack.

        :param int slot: slot number
        :return: location
        :rtype: Location
        """
        return Location(self._array[slot - 1])

    def reset_iterator(self):
        """Resets the iterator"""
        self._generator = self._yielditem()


class SlideHandler(PneumaticHolder, Item):
    def __init__(self,
                 location,
                 output,
                 x_offset=-37.5/2,
                 y_offset=-37.5/2-2,
                 height=None,
                 thickness=None,
                 clearheight=24.,
                 dist_to_pickup=75.-12.,
                 asset=assets.slide_handler,
                 base_asset=assets.slide_handler_base,
                 **kwargs,
                 ):
        """
        A slide handler held in the gripper of the robot

        :param location: index location of the docker
        :param dict installed: dictionary of installed pumps associated with each index
        :param int output: output number controlling the vacuum
        :param float x_offset: x offset from index location
        :param y_offset: y offset from index location
        :param height: height of the docker (mm)
        :param thickness: thickness of the dispenser module (mm)
        The gripper will go to height+thickness mm to pick up the dispenser

        :param clearheight: how high the dispenser must go up before it is clear of the docker (mm)
        :param dist_to_pickup: length from the center of the gripper to the pickup point of a slide (mm, radius of the dispenser)
        """
        PneumaticHolder.__init__(self, output=output, **kwargs)

        Item.__init__(
            self,
            location,
            length=thickness,
        )
        self._thickness = thickness

        if 'handler' not in robotarm.modules:  # check for handler in modules and install if not present
            robotarm.install_arm_module(
                name='handler',
                module_type='ArmModule',
                sh_el=robotarm.modules.gripper.upper,
                el_point=robotarm.modules.gripper.lower,
                length=robotarm.modules.gripper.length,
            )

        self._x_offset = x_offset  # x offset for base
        self._y_offset = y_offset  # y offset for base
        self._height = height  # height of base
        self._clearheight = clearheight
        self._dist_to_pickup = dist_to_pickup

        self.offset_location(  # offset location
            x=self._x_offset,
            y=self._y_offset,
            z=self._height,
        )
        self._safeheight = (  # safe clear height
            self._height
            + self._clearheight
            + self.length
        )
        self._home = None  # storage for home location
        self._slide = None  # storage attribute for slide

        self.base_asset = Asset(location, base_asset)
        self.asset = Asset(self._location, asset)

    def __repr__(self):
        return f'{self.__class__.__name__}({self.ingrip})'

    def __str__(self):
        return repr(self)

    def get_location(self, loc=None):
        """
        Returns either the bottom or pickup location of the dispenser. Top will return the
        location with vertical offset.

        :param 'bottom', 'pickup' loc: the location to retrieve
        :return: xyz location
        :rtype: Location
        """
        out = Location(self._location)  # stored location is the bottom of the dispenser
        if loc is None or loc == 'bottom':
            pass
        elif loc == 'pickup':
            out.offset(z=self._thickness)
        else:
            raise KeyError(f'The loc value "{loc}" is not recognized. Choose from "pickup" or "bottom"')
        return out

    def pick_up(self, height=None):
        """
        Pick up the dispenser module in the gripper

        :param height: optional safeheight
        :return:
        """
        if height is None or height < self._safeheight:
            height = self._safeheight
        robotarm.safeheight(height)
        robotarm.move_to_location(  # go to location
            self.get_location('pickup'),
            aligngripper='y',
        )
        # robotarm._gripper.lock_symmetry()
        self._home = robotarm.axes_location()  # retrieve exact home location
        robotarm.gripper_on(self)  # turn on gripper and store self in gripper module
        time.sleep(0.2)
        self.asset.attach('gripper')
        time.sleep(0.2)
        robotarm.modules.handler.item = self  # install self into handler arm module
        robotarm.safeheight(height)  # go to clearance height

    def put_down(self, height=None):
        """
        Put the dispenser module down.

        :param height: optional safeheight
        :return:
        """
        if height is None or height < self._safeheight:
            height = self._safeheight
        robotarm.safeheight(height)


        robotarm.move_axes(
            self._home,
            splitxyz='xyz',  # move xygr, then z
        )
        robotarm.gripper_off()  # turn off gripper and remove self from module
        time.sleep(0.2)
        self.asset.detach()
        time.sleep(0.2)
        robotarm.modules.handler.item = None  # remove self from handler arm module
        # robotarm._gripper.unlock_symmetry()
        robotarm.safeheight()

    def _calculate_offsets(self, angle, distance):
        """
        Calculates the appropriate offsets to apply to the arm module.

        :param angle: angle of the gripper (degrees)
        :return: lower extension, hoffset
        """
        angle = np.radians(angle)
        extension = distance * np.cos(angle)
        hoffset = distance * np.sin(angle)
        return extension, hoffset

    def _apply_lower(self, lower_extension):
        """
        Applies an extension to the length of the lower segment of the handler ArmModule

        :param lower_extension: extension (mm)
        """
        robotarm.modules.handler._lower = robotarm.modules.gripper.lower + lower_extension

    def pick_up_slide(self, slide, z_span=1.):
        """
        Picks up the provided slide.

        :param Slide slide: a slide instance
        :param z_span: z span to move up when picking up (mm)
        """
        if robotarm.modules.gripper.item is not self:
            raise ValueError('The slide handler is not in the robot arm grip. ')
        if isinstance(slide, Slide) is False:
            raise TypeError('The slide argument is expected to be a Slide instance.')
        initial = Location(slide.get_location())
        initial.offset(z=-self.length)  # todo check that this works
        final = Location(initial)
        initial.offset_vector(  # offset the initial location
            -(slide.span * 1.5 + self._dist_to_pickup),
            slide.orientation,
        )
        final.offset_vector(  # offset to pickup point
            -(slide.span / 2 - slide.pickup_point + self._dist_to_pickup),
            slide.orientation,
        )
        initial.offset(z=-z_span/2)  # offset initial downward
        final.offset(z=z_span/2)  # offset final upward
        # todo figure out how to use handler arm module for all methods that use the handler
        # extension, hoffset = self._calculate_offsets(
        #     slide.orientation + -np.degrees(robotarm._shoulder.radians + robotarm._elbow.radians),
        #     self._dist_to_pickup,
        # )
        # print(extension, hoffset)
        # self._apply_lower(extension)  # increase length by extension
        # robotarm.modules.handler.hoffset = hoffset  # apply horizontal offset
        robotarm.safeheight()
        robotarm.move_to_location(  # move to initial location, aligning the gripper
            initial,
            aligngripper='origin',
        )
        robotarm.move_to_location(  # move to final xy position
            final.xy,
            aligngripper='origin',
        )
        self.on(slide)  # turn on vacuum
        time.sleep(0.2)
        slide.item_asset.attach(self.asset.id)
        time.sleep(0.2)
        robotarm.move_to_location(  # move up
            final.dict('z'),
        )
        robotarm.move_to_location(  # move back while aligning gripper
            initial.xy,
            aligngripper='origin',
        )
        robotarm.safeheight()

    def put_down_slide(self, location, orientation=0., z_span=1.):
        """
        Puts the slide down in the specified location.

        :param location: location to place the center of the slide
        :param orientation: orientation for the slide
        :param z_span: z span to move down when placing (mm)
        :return: removed slide
        :rtype: Slide
        """
        if robotarm.modules.gripper.item is not self:
            raise ValueError('The slide handler is not in the robot arm grip. ')
        if self.ingrip is None:
            raise ValueError('There is nothing in the SlideHandler. ')
        initial = Location(location)
        initial.offset(z=-self.length)  # todo verify
        initial.offset_vector(
            -(self.ingrip.span / 2 - self.ingrip.pickup_point + self._dist_to_pickup),
            orientation,
        )
        final = Location(initial)
        initial.offset(z=z_span/2)
        final.offset(z=-z_span/2)
        # extension, hoffset = self._calculate_offsets(
        #     orientation,
        #     self._dist_to_pickup + self.ingrip.span / 2 - self.ingrip.pickup_point
        # )
        # self._apply_lower(extension)
        # robotarm.modules.handler.hoffset = hoffset
        robotarm.move_to_location(
            initial,
            aligngripper=orientation,
        )
        slide = self.off()  # turn off
        time.sleep(0.2)
        slide.item_asset.detach()
        time.sleep(0.2)
        slide.update_location(location)
        robotarm.move_to_location(
            final,
        )
        return slide

    def put_away_slide(self, location, orientation, z_span=1.):
        """
        Puts the slide away in a slot.

        :param location: location where the center of the slide will be in the slot
        :param orientation: orientation for the slide
        :param z_span: z span to move down when placing (mm)
        :return: removed slide
        :rtype: Slide
        """
        if robotarm.modules.gripper.item is not self:
            raise ValueError('The slide handler is not in the robot arm grip. ')
        if self.ingrip is None:
            raise ValueError('There is nothing in the SlideHandler. ')

        initial = Location(location)
        initial.offset(z=-self.length)  # todo verify
        final = Location(initial)
        initial.offset_vector(  # offset the initial location
            -(self.ingrip.span * 1.5 + self._dist_to_pickup),
            orientation,
        )
        final.offset_vector(  # offset to pickup point
            -(self.ingrip.span / 2 - self.ingrip.pickup_point + self._dist_to_pickup),
            orientation,
        )
        slide_location = Location(final)
        initial.offset(z=z_span / 2)  # offset initial upward
        final.offset(z=-z_span / 2)  # offset final downward
        # extension, hoffset = self._calculate_offsets(
        #     slide.orientation + -np.degrees(robotarm._shoulder.radians + robotarm._elbow.radians),
        #     self._dist_to_pickup,
        # )
        # print(extension, hoffset)
        # self._apply_lower(extension)  # increase length by extension
        # robotarm.modules.handler.hoffset = hoffset  # apply horizontal offset
        robotarm.safeheight()
        robotarm.move_to_location(  # move to initial location, aligning the gripper
            initial,
            aligngripper='origin',
        )
        robotarm.move_to_location(  # move to final xy position
            final.xy,
            aligngripper='origin',
        )
        slide = self.off()  # turn off vacuum
        robotarm.move_to_location(slide_location)
        slide.update_location(location)
        robotarm.move_to_location(  # move up
            final.dict('z'),
        )
        time.sleep(0.2)
        slide.item_asset.detach()
        time.sleep(0.2)
        robotarm.move_to_location(  # move back while aligning gripper
            initial.xy,
            aligngripper='origin',
        )
        robotarm.safeheight()
        return slide


class SpinCoaterCover(object):
    def __init__(self,
                 stand_index,
                 coater_index,
                 on_coater=False,
                 asset=None,
                 ):

        self.on_coater = on_coater
        self.axis_x_shift = -37.181
        self.axis_y_shift = -53.955

        self.safe_height = Location({'z': 160})

        # Stand positions

        self.at_cover_on_stand = Location(stand_index)
        self.at_cover_on_stand.update(z=78.22)
        self.at_cover_on_stand.offset(x=-0.22, y=-39.39)

        self.above_cover_on_stand = Location(stand_index)
        self.above_cover_on_stand.update(z=117.)
        self.above_cover_on_stand.offset(x=-0.22, y=-39.39)

        self.drop_cover_on_stand = Location(stand_index)
        self.drop_cover_on_stand.update(z=80.)
        self.drop_cover_on_stand.offset(x=-0.22, y=-39.39)

        # Coater positions

        self.at_cover_on_coater = Location(coater_index)
        self.at_cover_on_coater.update(z=108.)
        self.at_cover_on_coater.offset(x=self.axis_x_shift, y=self.axis_y_shift)

        self.above_cover_on_coater = Location(coater_index)
        self.above_cover_on_coater.update(z=160.)
        self.above_cover_on_coater.offset(x=self.axis_x_shift, y=self.axis_y_shift)

        self.drop_cover_on_coater = Location(coater_index)
        self.drop_cover_on_coater.update(z=109.)
        self.drop_cover_on_coater.offset(x=self.axis_x_shift, y=self.axis_y_shift)

        asset_location = self.drop_cover_on_coater if on_coater else self.drop_cover_on_stand
        self.asset = Asset(asset_location, asset)

    def move_from_stand_to_coater(self):

        if self.on_coater == True: # Test that cover is not on coater
            raise ValueError('Cover is already on the coater.')

        robotarm.move_to_location(self.safe_height, aligngripper='y')
        # robotarm.move_to_location(self.above_cover_on_stand, aligngripper='y')
        robotarm.move_to_location(self.at_cover_on_stand, aligngripper='y')
        robotarm.gripper.on() # Grip the cover
        time.sleep(0.2)
        self.asset.attach('gripper')
        time.sleep(0.2)
        robotarm.move_to_location(self.safe_height, aligngripper='y') # Move to safe height
        robotarm.move_to_location(self.above_cover_on_coater, aligngripper='y')
        robotarm.move_to_location(self.drop_cover_on_coater, aligngripper='y')
        robotarm.gripper.off() # Drop the cover on the coater
        time.sleep(0.2)
        self.asset.detach()
        time.sleep(0.2)
        robotarm.move_to_location(self.safe_height, aligngripper='y')

        self.on_coater = True # Set cover as on coater

    def move_from_coater_to_stand(self):

        if self.on_coater == False: # Test to make sure cover is on coater
            raise ValueError('Cover is not on the coater.')

        robotarm.move_to_location(self.safe_height, aligngripper='y')
        robotarm.move_to_location(self.above_cover_on_coater, aligngripper='y')
        robotarm.move_to_location(self.at_cover_on_coater, aligngripper='y')
        robotarm.gripper.on() # Pick up the cover
        time.sleep(0.2)
        self.asset.attach('gripper')
        time.sleep(0.2)
        robotarm.move_to_location(self.safe_height, aligngripper='y')
        # robotarm.move_to_location(self.above_cover_on_stand, aligngripper='y')
        robotarm.move_to_location(self.drop_cover_on_stand, aligngripper='y')
        robotarm.gripper.off() # Drop off the cover on the coater
        time.sleep(0.2)
        self.asset.detach()
        time.sleep(0.2)
        robotarm.move_to_location(self.safe_height, aligngripper='y')

        self.on_coater = False


class SpinCoaterSuction(PneumaticHolder):
    def __init__(self,
                 output,
                 **kwargs,
                 ):

        PneumaticHolder.__init__(self, output=output, **kwargs)


